package com.example.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.model.Prescription;


@Service
public interface PrescriptionService {
	 List<Prescription> listAllPrescription();
	 Prescription getPrescriptionById(Integer id);
	 Prescription savePrescription(Prescription prescription);
	 void deletePrescription(Integer id);
}
