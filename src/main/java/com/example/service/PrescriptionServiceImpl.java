package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.example.model.Prescription;
import com.example.repository.PrecriptionRepository;

@Service
public class PrescriptionServiceImpl implements PrescriptionService {
	
	@Autowired
	@Qualifier("precriptionRepository")PrecriptionRepository precriptionRepository ;

	@Override
	public List<Prescription> listAllPrescription() {
		// TODO Auto-generated method stub
		return precriptionRepository.findAll();
	}

	@Override
	public Prescription getPrescriptionById(Integer id) {
		// TODO Auto-generated method stub
		return precriptionRepository.findOne(id);
	}

	

	@Override
	public void deletePrescription(Integer id) {
		precriptionRepository.delete(id);
		
	}

	@Override
	public Prescription savePrescription(Prescription prescription) {
		// TODO Auto-generated method stub
		return precriptionRepository.save(prescription);
	}
}
