package com.example.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "prescription")
public class Prescription {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "visiting_date")
	private Date visitingDate;
	
	@Column(name = "next_visiting_date")
	private Date nextVisitingDate;
	
	@Column(name = "name")
	@NotEmpty(message = "*Please Enter your Name")
	private String name;
	
	
	@Column(name = "age")
	@NotNull @Min(0) @Max(100)
	private Integer age=0;
	
	
	
	public Date getVisitingDate() {
		return visitingDate;
	}
	public void setVisitingDate(Date visitingDate) {
		this.visitingDate = visitingDate;
	}
	public Date getNextVisitingDate() {
		return nextVisitingDate;
	}
	public void setNextVisitingDate(Date nextVisitingDate) {
		this.nextVisitingDate = nextVisitingDate;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	@Column(name = "gender")
	private String gender;
	
	
	@Column(name = "diagonosis")
	private String diagonosis;
	/*@Column (name = "next_visiting_date")
	private Date nextVisitingDate;
	@Column (name = "current_date")
	private Date currentDate;*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDiagonosis() {
		return diagonosis;
	}
	public void setDiagonosis(String diagonosis) {
		this.diagonosis = diagonosis;
	}
	/*public Date getNextVisitingDate() {
		return nextVisitingDate;
	}
	public void setNextVisitingDate(Date nextVisitingDate) {
		this.nextVisitingDate = nextVisitingDate;
	}
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}*/
	/*@Override
	public String toString() {
		return "Prescription [id=" + id + ", visitingDate=" + visitingDate + ", nextVisitingDate=" + nextVisitingDate
				+ ", name=" + name + ", age=" + age + ", gender=" + gender + ", diagonosis=" + diagonosis + "]";
	}
	*/


}
