package com.example.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.model.Prescription;
import com.example.service.PrescriptionService;

@Controller
@RequestMapping("/Admin/prescription")
public class PrescriptionController {

	PrescriptionService prescriptionService;

	@Autowired
	public void setPrescriptionService(PrescriptionService prescriptionService) {
		this.prescriptionService = prescriptionService;
	}

	//This method used for view all data in a tabular form.
	@RequestMapping(value = "/listOfAllPrescription")
	public String prescriptionView(Model model) {
		List<Prescription> prescriptionList = prescriptionService.listAllPrescription();
		model.addAttribute("prescriptionList", prescriptionList);

		return "prescription_view";
	}

	

	//This method used for add a new data.
	@RequestMapping("/addPrescription")
	public String savepes(Model model) {
		Prescription prescription = new Prescription();
		model.addAttribute("prescription", prescription);
		return "add_new_prescription";

	}

	
	//This method used for save a new data.
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String savePrescription(Prescription prescription) {
		prescriptionService.savePrescription(prescription);
		return "redirect:/Admin/prescription/listOfAllPrescription";
	}
	
	
	//This method used for update specific data by its id.
	@RequestMapping("/update/{id}")
	public String updatetPes(Model model, @PathVariable Integer id) {
		Prescription prescription = prescriptionService.getPrescriptionById(id);
		model.addAttribute("prescription", prescription);
		return "json_date_in_tabuler_for";
	}
	
	
	//This method used for delete specific data by its id.
	@RequestMapping("/delete/{id}")
	public String deletePes(Model model, @PathVariable Integer id) {
		prescriptionService.deletePrescription(id);
		return "redirect:/Admin/prescription/listOfAllPrescription";
	}

	//This method used for View data wise report.
	@RequestMapping("/reportView")
	public String dateWiseReport(Model model) {
		List<Prescription> prescriptionList = prescriptionService.listAllPrescription();
		Map<String, Integer> report = new HashMap<>();
		for (Prescription i : prescriptionList) {
			int count = 0;
			for (Prescription m : prescriptionList) {
				if (i.getVisitingDate().equals(m.getVisitingDate())) {
					count = count + 1;
					System.out.println("Date " + i.getVisitingDate() + " " + count);
				}
			}
			System.out.println("Date " + i.getVisitingDate() + " " + count);
			report.put(i.getVisitingDate().toString(), count);
		}
		model.addAttribute("report", report);
		return "report_count";
	}
	
	//This method used for view JSON Data.
	@RequestMapping("/JsonTable")
	public String JSONView(Model model) {
		
		return "json_date_in_tabuler_for";
		
	}
	

}
