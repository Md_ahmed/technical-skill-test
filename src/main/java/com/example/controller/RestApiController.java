package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Prescription;
import com.example.service.PrescriptionService;

@RestController
@RequestMapping("/Admin/Rest")
public class RestApiController {

	PrescriptionService prescriptionService;

	@Autowired
	public void setPrescriptionService(PrescriptionService prescriptionService) {
		this.prescriptionService = prescriptionService;
	}
	
	@RequestMapping(value = "/listOfAllPrescription")
	public List<Prescription> prescriptionView(Model model){
		System.out.println("Hi Mta I am now here.////llllll------\n" );
		List<Prescription> prescriptionList= prescriptionService.listAllPrescription();
		model.addAttribute("prescriptionList",prescriptionList);
		
		return prescriptionList;
	}
	
	@RequestMapping(value = "/home")
	public String check(){
		return "/admin/home";
	}
	@RequestMapping("/addPrescription")
	public String savepes(Model model){
		Prescription prescription = new Prescription();
		model.addAttribute("prescription", prescription);
		System.out.println("Hi Mta I am now here.////llllll------\n");
		return "add_new_prescription";
		
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String savePrescription(Prescription prescription) {
		//prescription.setCurrentDate(new Date());
		//prescription.setId(1);
		System.out.println(prescription.toString());
		prescriptionService.savePrescription(prescription);
		return "redirect:/Admin/lostOfAllPrescription";
	}
	
	@RequestMapping("/update/{id}")
	public Prescription updatetPes(Model model, @PathVariable Integer id) {
		Prescription prescription = prescriptionService.getPrescriptionById(id);
		model.addAttribute("prescription", prescription);
		return prescription;
	}
	
	@RequestMapping("/delete/{id}")
	public String  deletePes(Model model, @PathVariable Integer id){
		//logger.info("Hi mt_ahmed I am in Delete Method");
		prescriptionService.deletePrescription(id);
	
		
		return "redirect:/Admin/prescription/lostOfAllPrescription";
	}

}

